#include <stdio.h>
#include <stdlib.h>

char* read_file(const char* fileName, size_t* len)
{
	FILE *fp;
	long lSize;
	char *buffer;

	fp = fopen ( fileName , "rb" );
	if( !fp ) perror(fileName),exit(1);

	fseek( fp , 0L , SEEK_END);
	lSize = ftell( fp );
	if(len) (*len) = lSize;
	rewind( fp );

	buffer = (char*)calloc( 1, lSize+1 );
	if( !buffer ) fclose(fp),fputs("memory alloc fails",stderr),exit(1);

	if( 1!=fread( buffer , lSize, 1 , fp) )
		fclose(fp),free(buffer),fputs("entire read fails",stderr),exit(1);

	fclose(fp);
	return buffer;
}

void xor_file(const char* srcPath, const char* dstPath, const char* k, const unsigned long keyLen)
{	
	FILE* pSrc = fopen(srcPath, "rb");
	FILE* pDst = fopen(dstPath, "wb");
	unsigned long len;
	unsigned long cur = 0;
	char ch;

	if( !pDst ) perror(srcPath), exit(1);	
	if( !pDst ) perror(dstPath), exit(1);			

	_fseeki64(pSrc, 0, SEEK_END);
	len=_ftelli64(pSrc);
	_fseeki64(pSrc, 0, SEEK_SET);		

	while(len-- > 0)
	{
		fread(&ch, 1, 1, pSrc);
		ch = ch ^ k[cur++ % keyLen];
		fwrite(&ch,1,1,pDst);
	}

	fclose(pSrc);		
	fclose(pDst);		
}

int main(int argc, char* argv[])
{
	if(argc != 4)
	{
		printf("Usage:\n\ [KEYFILE] [SOURCE] [DESTINATION]\n");
		return 0;
	}
	else
	{
		size_t keyLen = 0;
		char* xorKey = read_file(argv[1],&keyLen);
		xor_file(argv[2],argv[3],xorKey,keyLen);
	}
}