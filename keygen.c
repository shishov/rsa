#include <stdio.h>
#include "mpir/gmp.h"
#include <time.h>
#include <stdlib.h>

#define BITSTRENGTH 1024 //bits
#define XOR_KEY_SIZE 64 //bites
#define E "65537"
#define SECRET_RSA_EXTENSION ".sk"
#define PUBLIC_RSA_EXTENSION ".pk"
#define XOR_KEY_EXTENSION ".xk"
#define MAX_FILENAME 1024

void rsa_gen(mpz_t e, mpz_t d, mpz_t n)
{
	mpz_t p; // LEN-sized prime
	mpz_t q; // LEN-sized prime
	mpz_t tol; // euler tolient tol = (p-1)(q-1)
	mpz_t tmp1; 
	mpz_t tmp2; 
    gmp_randstate_t r_state;   
    time_t rawtime;
	gmp_randstate_t rands;

	mpz_init(p);
	mpz_init(q); 
	mpz_init(tol);
	mpz_init(tmp1);
	mpz_init(tmp2);
	
	gmp_randinit_default (rands);
	mpz_urandomb (p,rands,BITSTRENGTH);
	mpz_urandomb (q,rands,BITSTRENGTH);

    mpz_nextprime(p,p);
    mpz_nextprime(q,q);
    mpz_mul(n,p,q);

    // compute euler tolient
    mpz_sub_ui(tmp1, p, 1);
    mpz_sub_ui(tmp2, q, 1);
    mpz_mul(tol,tmp1,tmp2);

    // this can be rewrited
    // to manual search of GCD of e and tol, but 65537 is ok
    mpz_set_str(e, E, 10);    

    // d = modular multiplicative inverse of e mod euler tolient 
    mpz_invert(d,e,tol); 

    mpz_clear(p);
    mpz_clear(q);    
    mpz_clear(tmp1);
    mpz_clear(tmp2);
    mpz_clear(tol);
}

char* generate(const size_t size)
{
	char* buffer = (char*)malloc(size);
	int i = 0;
	srand(time(0));

	memset(buffer,0,size);

	for(;i<size;i++)	
		buffer[i] = rand() % 256;

	return buffer;
}

int main(int argc, char* argv[])
{	
	if(argc != 3)
	{
		printf("Usage:\n\tplainrsa [rsa/xor] [KEYNAME]\n");
		return 0;
	}
	else
	{
		if(memcmp(argv[1],"rsa",3) == 0)
		{
			FILE* pSec;
			FILE* pPub;
			mpz_t e;
			mpz_t d;
			mpz_t n;
			char* secFName = (char*)malloc(MAX_FILENAME);
			char* pubFName = (char*)malloc(MAX_FILENAME);
			char* eStr;
			char* dStr;
			char* nStr;

			strcpy(secFName, argv[2]);
			strcpy(pubFName, argv[2]);
			strcat(secFName, SECRET_RSA_EXTENSION);		
			strcat(pubFName, PUBLIC_RSA_EXTENSION);

			pSec = fopen(secFName, "w");
			pPub = fopen(pubFName, "w");

			if( !pSec ) perror(secFName), exit(1);
			if( !pPub ) perror(pubFName), exit(1);

			mpz_init(e); // e = E
			mpz_init(d); // d = modular multiplicative inverse e  mod euler tolient 	
			mpz_init(n); // modulus n = p * q	
			rsa_gen(e,d,n);


			eStr = mpz_get_str((char *) NULL, 16, e);
			dStr = mpz_get_str((char *) NULL, 16, d);
			nStr = mpz_get_str((char *) NULL, 16, n);

			fprintf(pSec,"%s\n%s", dStr, nStr);   
			fprintf(pPub,"%s\n%s", eStr, nStr);   

			fclose(pSec), fclose(pPub);
			free(eStr), free(dStr), free(nStr), free(secFName), free(pubFName);
			mpz_clear(e), mpz_clear(d), mpz_clear(n);
		}
		else if(memcmp(argv[1],"xor",3) == 0)
		{
			FILE* pKey;
			char* fileName = (char*)malloc(MAX_FILENAME);
			char* xorKey = generate(XOR_KEY_SIZE);

			strcpy(fileName, argv[2]);
			strcat(fileName, XOR_KEY_EXTENSION);	

			pKey = fopen(fileName, "wb");
			if( !pKey ) perror(fileName), exit(1);

			fwrite(xorKey,XOR_KEY_SIZE,1,pKey);
			fclose(pKey);
			free(fileName), free(xorKey);
		}
		else
		{
			printf("Usage:\n\tplainrsa [rsa/xor] [KEYNAME]\n");
			return 0;
		}
		
	    return 0;		
	}
}