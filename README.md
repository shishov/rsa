## Instalation ##

 1. Install GMP (http://gmplib.org/manual/Installing-GMP.html#Installing-GMP)
 2. Compile plainrsa `gcc plainrsa.c -lgmp -o plainrsa`
 3. Compile keygen `gcc keygen.c -lgmp -o keygen`

## Usage ##
Keygen will create 2 keys stored in files

    keygen [SECRETKEY] [PUBLICKEY] 

Plain rsa will encode or decode source file to destination (depends on key)

    plainrsa [KEYFILE] [SOURCE] [DESTINATION]   

## Key Format ##
Public key format
	
	(%s\n%s, e, n), e - secret exponent, n - modulus

Secret key format
	
	(%s\n%s, d, n), d - secret exponent, n - modulus