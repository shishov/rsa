echo off
cd Debug
echo Generating xor key
keygen.exe xor xorKey
pause

echo Generating rsakey
keygen.exe rsa rsaKey
pause

echo Crypt xor key
plainrsa.exe rsaKey.pk xorKey.xk xorkey.dat
pause

echo Xor the file
xor.exe xorKey.xk keygen.exe keygen.dat
pause