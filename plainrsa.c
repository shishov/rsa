#include <stdio.h>
#include "mpir/gmp.h"
#include <stdlib.h>

#define MAX_RSA_KEY_LEN 2024
#define RSA_KEY_BASE 16
#define RSA_KEY_FORMAT "%s\n%s"

typedef struct { mpz_t e; mpz_t n; } key;

char* crypt(const char* pSrc, size_t sizeIn, const key key, size_t* sizeOut)
{
	mpz_t src;
	mpz_t dst;
	char* out;
	mpz_init(src);
	mpz_init(dst);
	mpz_import(src, sizeIn, -1, sizeof(char), 0, 0, pSrc);
	mpz_powm(dst, src, key.e, key.n);
	*sizeOut = mpz_sizeinbase (dst, 2) / 8 + 1;
	out = (char*) malloc(*sizeOut);
	memset(out,0,*sizeOut);
	mpz_export(out, NULL, 0, *sizeOut, 0, 0, dst);
	mpz_clear(src), mpz_clear(dst);
	return out;
}

key load_key(const char* fileName)
{	
	FILE* f = fopen(fileName, "rb");
	key key;
	char* expStr;
	char* modStr;

	if( !f ) perror(fileName),exit(1);
	
	expStr = (char*)malloc(MAX_RSA_KEY_LEN);
	modStr = (char*)malloc(MAX_RSA_KEY_LEN);

	if( !expStr || !modStr ) fclose(f),fputs("memory alloc fails",stderr),exit(1);
	
	fscanf(f, RSA_KEY_FORMAT, expStr, modStr);	
	fclose(f);

	mpz_init_set_str (key.e, expStr, RSA_KEY_BASE);
	mpz_init_set_str (key.n, modStr, RSA_KEY_BASE);	

	free(expStr), free(modStr);
	return key;	
}

char* read_file(const char* fileName, size_t* len)
{
	FILE *fp;
	long lSize;
	char *buffer;

	fp = fopen ( fileName , "rb" );
	if( !fp ) perror(fileName),exit(1);

	fseek( fp , 0L , SEEK_END);
	lSize = ftell( fp );
	if(len) (*len) = lSize;
	rewind( fp );
	
	buffer = (char*)calloc( 1, lSize+1 );
	if( !buffer ) fclose(fp),fputs("memory alloc fails",stderr),exit(1);
	
	if( 1!=fread( buffer , lSize, 1 , fp) )
	  fclose(fp),free(buffer),fputs("entire read fails",stderr),exit(1);

	fclose(fp);
	return buffer;
}

void write_file(const char* fileName, size_t len, char* out)
{
	FILE* f = fopen(fileName, "wb");
	if( !f ) perror(fileName), exit(1);	
	fwrite(out, len, 1, f);	
	fclose(f);
}

int main(int argc, char* argv[])
{
	if(argc != 4)
	{
		printf("Usage:\n\tplainrsa.exe [KEYFILE] [SOURCE] [DESTINATION]\n");
		return 0;
	}
	else
	{	
		key k = load_key(argv[1]);		
		char* src;
		char* dst;
		size_t srcLen = 0;
		size_t dstLen = 0;

		src = read_file(argv[2],&srcLen);
		dst = crypt(src,srcLen,k,&dstLen);
		write_file(argv[3],dstLen,dst);
		
		free(src), free(dst);
		mpz_clear(k.e), mpz_clear(k.n);
		return 0;
	}
}